# Frontpage Per Lang

The Front Page Per Language module allows to configure the front page per language.

- For a full description of the module visit the [project page](https://www.drupal.org/project/frontpage_per_lang).

- To submit bug reports and feature suggestions, or to track changes visit the project's [issue queue](https://www.drupal.org/project/issues/frontpage_per_lang).

## Table of contents

- Requirements
- Installation
- Current Maintainers

## Requirements

This projects requires the following core module:
[Multilingual guide](https://www.drupal.org/docs/administering-a-drupal-site/multilingual-guide)

## Installation

Install as you would normally install a contributed Drupal module.
Visit <https://www.drupal.org/node/1897420> for further information.

## Current Maintainers

- Sonvir (sonvir249) - <https://www.drupal.org/u/sonvir249>
