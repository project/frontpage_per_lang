<?php

namespace Drupal\frontpage_per_lang;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for service to functionality to set front page per language.
 */
interface LanguageFrontPageInterface {

  /**
   * Adds form element/input to basic site settings form.
   *
   * @param array $form
   *   Form render array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Settings form state.
   */
  public function addLanguageFrontPageElement(array &$form, FormStateInterface $formState);

  /**
   * Adds validation to basic site settings form.
   *
   * @param array $form
   *   Form render array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Settings form state.
   */
  public function validateLanguageFrontPageElement(array &$form, FormStateInterface $formState);

  /**
   * Updates form basic site settings.
   *
   * @param array $form
   *   Form render array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Settings form state.
   */
  public function updateLanguageFrontPageElement(array &$form, FormStateInterface $formState);

}
