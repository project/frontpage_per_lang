<?php

namespace Drupal\frontpage_per_lang\Path;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\PathMatcher;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Overrides the PathMatcher class.
 */
class PathMatcherAlter extends PathMatcher {
  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new PathMatcherAlter object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteMatchInterface $route_match, LanguageManagerInterface $languageManager) {
    $this->configFactory = $config_factory;
    $this->routeMatch = $route_match;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function isFrontPage() {
    // Cache the result as this is called often.
    if (!isset($this->isCurrentFrontPage)) {
      $front_page = $this->getFrontPagePath();
      $language_id = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
      $default_language_id = $this->languageManager->getDefaultLanguage()->getId();
      if ($language_id !== $default_language_id) {
        $language_id = str_replace('-', '', $language_id);
        if ($this->configFactory->get('system.site')->get('page.front_' . $language_id)) {
          $front_page = $this->configFactory->get('system.site')->get('page.front_' . $language_id);
        }
      }

      $this->isCurrentFrontPage = FALSE;
      // Ensure that the code can also be executed when there is no active
      // route match, like on exception responses.
      if ($this->routeMatch->getRouteName()) {
        $url = Url::fromRouteMatch($this->routeMatch);
        $this->isCurrentFrontPage = ($url->getRouteName() && '/' . $url->getInternalPath() === $front_page);
      }
    }
    return $this->isCurrentFrontPage;
  }

}
