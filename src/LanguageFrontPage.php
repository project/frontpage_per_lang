<?php

namespace Drupal\frontpage_per_lang;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\path_alias\AliasManagerInterface;

/**
 * Class to alter basic site information form.
 *
 * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
 *   The factory for configuration objects.
 * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
 *   The typed config manager.
 * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
 *   The language manager.
 * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
 *   The path validator.
 * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
 *   The path alias manager.
 */
class LanguageFrontPage implements LanguageFrontPageInterface {
  use StringTranslationTrait;

  /**
   * Constructs a new LanguageFrontPage object.
   */
  public function __construct(public ConfigFactoryInterface $configFactory, public TypedConfigManagerInterface $typedConfigManager, public LanguageManagerInterface $languageManager, public PathValidatorInterface $pathValidator, public AliasManagerInterface $aliasManager) {
  }

  /**
   * {@inheritDoc}
   */
  public function addLanguageFrontPageElement(array &$form, FormStateInterface $formState) {
    global $base_url;
    $language_negotiation = $this->configFactory->getEditable('language.negotiation');
    $site_config = $this->configFactory->getEditable('system.site');
    $languages = $this->languageManager->getLanguages();
    $prefixes = $language_negotiation->get('url.prefixes');
    if (count($languages) > 1) {
      foreach ($languages as $langcode => $language) {
        $language_id = str_replace('-', '', $language->getId());
        if (!$language->isDefault()) {
          $t_args = [
            '%language' => $language->getName(),
            '%langcode' => $language->getId(),
          ];
          $form['front_page']['site_frontpage_' . $language_id] = [
            '#type' => 'textfield',
            '#title' => $this->t('Default front page for %language (%langcode)', $t_args),
            '#maxlength' => 64,
            '#default_value' => $site_config->get('page.front_' . $language_id) ? $this->aliasManager->getAliasByPath($site_config->get('page.front_' . $language_id)) : '',
            '#field_prefix' => $base_url . '/' . $prefixes[$langcode] ?? '',
          ];
        }
      }
    }

    $form['#validate'][] = [$this, 'validateLanguageFrontPageElement'];
    $form['#submit'][] = [$this, 'updateLanguageFrontPageElement'];

  }

  /**
   * {@inheritDoc}
   */
  public function validateLanguageFrontPageElement(array &$form, FormStateInterface $formState) {
    $languages = $this->languageManager->getLanguages();
    foreach ($languages as $language) {
      if (!$language->isDefault()) {
        $language_id = str_replace('-', '', $language->getId());
        // Get the normal path of the front page.
        $formState->setValueForElement($form['front_page']['site_frontpage_' . $language_id], $this->aliasManager->getPathByAlias($formState->getValue('site_frontpage_' . $language_id)));
        // Validate front page path.
        if (($value = $formState->getValue('site_frontpage_' . $language_id)) && $value[0] !== '/') {
          $formState->setErrorByName('site_frontpage_' . $language_id, $this->t("The path '%path' has to start with a slash.", ['%path' => $formState->getValue('site_frontpage_' . $language_id)]));
        }
        if (!$this->pathValidator->isValid($formState->getValue('site_frontpage_' . $language_id))) {
          $formState->setErrorByName('site_frontpage_' . $language_id, $this->t("Either the path '%path' is invalid or you do not have access to it.", ['%path' => $formState->getValue('site_frontpage_' . $language_id)]));
        }
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function updateLanguageFrontPageElement(array &$form, FormStateInterface $formState) {
    $languages = $this->languageManager->getLanguages();
    $site_config = $this->configFactory->getEditable('system.site');
    foreach ($languages as $language) {
      if (!$language->isDefault()) {
        $language_id = str_replace('-', '', $language->getId());
        $site_config->set('page.front_' . $language_id, $formState->getValue('site_frontpage_' . $language_id));
      }
    }
    $site_config->save();
  }

}
