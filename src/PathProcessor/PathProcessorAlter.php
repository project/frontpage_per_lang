<?php

namespace Drupal\frontpage_per_lang\PathProcessor;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PathProcessor\PathProcessorFront;
use Symfony\Component\HttpFoundation\Request;

/**
 * Overrides the PathProcessorFront class.
 */
class PathProcessorAlter extends PathProcessorFront {
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new PathProcessorAlter object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory, LanguageManagerInterface $languageManager) {
    $this->configFactory = $configFactory;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {

    if ($path === '/') {
      $language_id = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
      $default_language_id = $this->languageManager->getDefaultLanguage()->getId();
      $path = $this->configFactory->get('system.site')->get('page.front');
      if ($language_id !== $default_language_id) {
        $language_id = str_replace('-', '', $language_id);
        if ($this->configFactory->get('system.site')->get('page.front_' . $language_id)) {
          $path = $this->configFactory->get('system.site')->get('page.front_' . $language_id);
        }
      }

      if (empty($path)) {
        // Return parent.
        return parent::processInbound($path, $request);
      }
    }
    return $path;
  }

}
